use minilinks;
CREATE TABLE links (
  handle VARCHAR(32) PRIMARY KEY NOT NULL,  
  link VARCHAR(512) NOT NULL,
  visits INT DEFAULT 0
);
