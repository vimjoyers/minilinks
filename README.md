# Minilinks
@csyankovskyi


# Про проєкт
Сервіс скорочення URL, написаний на Go. 

# Інструкція по запуску:
```
git clone https://gitlab.com/vimjoyers/minilinks
cd minilinks
cp .env.example .env && vim .env
docker compose up
```
# Інструкція по користуванню

- `/api/link/new` POST, headers: {Authorization: "Bearer {токен в .env}"}  {handle: "скорочений", link: "лінк"} - створити лінк
- `/api/link/edit` POST, headers: {Authorization: "Bearer {токен в .env}"}  {handle: "скорочений", link: "новий лінк"} - змінити лінк
- `/api/link/delete/[скорочений]` GET, headers: {Authorization: "Bearer {токен в .env}"} - видалити лінк
- `/api/link/info` GET, headers: {Authorization: "Bearer {токен в .env}"} - статистика лінків

### TODO:
- Admin UI  
