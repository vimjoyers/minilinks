package main

import (
	"database/sql"
	"log"
  "encoding/json"
  "strings"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"net/http"
	"time"
  "os"
)

type linkJSON struct {
  Handle string
  Link string
}
func main() {
  log.Printf("Starting")
  cfg := mysql.Config{
      User:   os.Getenv("MYSQL_USER"),
      Passwd: os.Getenv("MYSQL_PASSWORD"),
      Net:    "tcp",
      Addr:   "db:3306",
      DBName: os.Getenv("MYSQL_DATABASE"),
  }
  db, err := sql.Open("mysql", cfg.FormatDSN())
  if err != nil {
      log.Fatal(err)
  }
  pingErr := db.Ping()
  if pingErr != nil {
      log.Fatal(pingErr)
  }  // See "Important settings" section.
  db.SetConnMaxLifetime(time.Minute * 3)
  db.SetMaxOpenConns(10)
  db.SetMaxIdleConns(10)
  log.Printf("DB connected")
	r := mux.NewRouter()
  r.HandleFunc("/admin", adminHandler)
	r.HandleFunc("/api/link/new", newLinkHandler(db)).Methods("POST", "OPTIONS")
	r.HandleFunc("/api/link/info", getLinkHandler(db)).Methods("GET", "OPTIONS")
	r.HandleFunc("/api/link/delete/{link}", deleteLinkHandler(db)).Methods("GET", "OPTIONS")
	r.HandleFunc("/api/link/edit", editLinkHandler(db)).Methods("POST", "OPTIONS")
	r.HandleFunc("/{link}", miniHandler(db))
  addr := fmt.Sprintf(":%s", os.Getenv("PORT"))
	srv := &http.Server{
		Handler: r,
		Addr:    addr,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
func miniHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
  return func(w http.ResponseWriter, r *http.Request) {
  log.Print("miniHandler")
  vars := mux.Vars(r)
  link, err := GetLink(db, vars["link"])
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    fmt.Fprintln(w, err)
    return
  }
  w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
  w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
  w.Header().Set("Content-Type", "application/json")
  http.Redirect(w, r, *link, http.StatusSeeOther) 
}}
func adminHandler(w http.ResponseWriter, r *http.Request) {
  w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
  w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
  http.Redirect(w, r, "http://127.0.0.1:3000", http.StatusMovedPermanently)
}

func newLinkHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
return  func(w http.ResponseWriter, r *http.Request) {
  authorization := r.Header.Get("Authorization")
	idToken := strings.TrimSpace(strings.Replace(authorization, "Bearer", "", 1))
  if idToken != os.Getenv("AUTH_TOKEN") {
    w.WriteHeader(http.StatusForbidden)
    return
  }
  log.Print("newLinkHandler")
  var l linkJSON
  if err := json.NewDecoder(r.Body).Decode(&l); err != nil {
    w.WriteHeader(http.StatusBadRequest)
    fmt.Fprintln(w, err)
    return
  }
  log.Printf("%s %s", l.Handle, l.Link)
  if err := NewLink(db, l.Handle, l.Link); err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    fmt.Fprintln(w, err)
    return
  }  
  w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
  w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
  w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, r)
}}
func deleteLinkHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
return func(w http.ResponseWriter, r *http.Request) {
  authorization := r.Header.Get("Authorization")
	idToken := strings.TrimSpace(strings.Replace(authorization, "Bearer", "", 1))
  if idToken != os.Getenv("AUTH_TOKEN") {
    w.WriteHeader(http.StatusForbidden)
    return
  }
  vars := mux.Vars(r)
  if err := DeleteLink(db, vars["handle"]); err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    fmt.Fprintln(w, err)
    return
  }  
  w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
  w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
  w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, r)
}
}
func editLinkHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
return func(w http.ResponseWriter, r *http.Request) {
  authorization := r.Header.Get("Authorization")
	idToken := strings.TrimSpace(strings.Replace(authorization, "Bearer", "", 1))
  if idToken != os.Getenv("AUTH_TOKEN") {
    w.WriteHeader(http.StatusForbidden)
    return
  }
  var l linkJSON
  if err := json.NewDecoder(r.Body).Decode(&l); err != nil {
    w.WriteHeader(http.StatusBadRequest)
    fmt.Fprintln(w, err)
    return
  }
  if err := EditLink(db, l.Handle, l.Link); err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    fmt.Fprintln(w, err)
    return
  }  
  w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
  w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
  w.Header().Set("Content-Type", "application/json")
	fmt.Fprintln(w, r)
}}
func getLinkHandler(db *sql.DB) func(w http.ResponseWriter, r *http.Request) {
return func(w http.ResponseWriter, r *http.Request) {
  authorization := r.Header.Get("Authorization")
	idToken := strings.TrimSpace(strings.Replace(authorization, "Bearer", "", 1))
  if idToken != os.Getenv("AUTH_TOKEN") {
    w.WriteHeader(http.StatusForbidden)
    return
  }
  links, err := GetLinkInfo(db)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  res, err := json.Marshal(links)
  if err != nil {
    panic(err)
  }
  w.Header().Set("Access-Control-Allow-Origin", "http://localhost:3000")
  w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
  w.Header().Set("Content-Type", "application/json")
  fmt.Fprintln(w, string(res))
}}
type Minilink struct {
	Handle string
	Link   string
	Visits uint
}

func GetLinkInfo(db *sql.DB) ([]Minilink, error) {
  var lnks []Minilink
  rows, err := db.Query("SELECT * FROM links;") 
  if err != nil {
    panic(err)
  }
  defer rows.Close()
  var lnk Minilink
  for rows.Next() {
    if err := rows.Scan(&lnk.Handle, &lnk.Link, &lnk.Visits); err != nil {
      return nil, fmt.Errorf("GetLink: %v", err)
    }
    lnks = append(lnks, lnk)
  }
  if err := rows.Err(); err != nil {
    return nil, fmt.Errorf("GetLink: %v",  err)
  }
  return lnks, nil
}
func GetLink(db *sql.DB, handle string) (*string, error) {
  log.Print("GetLink")
  rows, err := db.Query("SELECT link, visits FROM links WHERE handle=?;", handle) 
  if err != nil {
    panic(err)
  }
  defer rows.Close()
  var link string
  var visits uint
  rows.Next()
  if err := rows.Scan(&link, &visits); err != nil {
    return nil, fmt.Errorf("GetLink %q: %v", handle, err)
  }
  if err := rows.Err(); err != nil {
    return nil, fmt.Errorf("GetLink %q: %v", handle, err)
  }
  if _, err := db.Query("UPDATE links SET visits = ? WHERE handle = ?;", visits+1, handle); err != nil {
    return nil, err
  }
  return &link, nil
}

func NewLink(db *sql.DB, handle string, link string) (error) {
  log.Print("newLinkHandler")
  log.Printf("%s %s",handle, link)
  rows, err := db.Query("INSERT INTO links(handle, link) VALUES (?, ?);", handle, link)
  defer rows.Close()
  if err != nil { 
    return err
  } 
  return nil
}
func DeleteLink(db *sql.DB, handle string) (error) {
  if _, err := db.Query("DELETE FROM links WHERE handle = ?;", handle); err != nil {
    return err
  } 
  return nil
}

func EditLink(db *sql.DB, handle string, link string) (error) {
  if _, err := db.Query("UPDATE links SET link = ? WHERE handle = ?;", link, handle); err != nil {
    return err
  } 
  return nil

}
